extern crate num;
extern crate rand;
#[macro_use] extern crate serde_derive;

use num::{Zero, One};
use num::{BigUint, BigInt};
use num::bigint::{self, ToBigInt, ToBigUint, RandBigInt};
use rand::prelude::*;


const KEY_BITS: usize = 1024;

#[derive(Serialize, Deserialize)]
pub struct PublicKey {
    n: BigUint,
    e: BigUint,
}

pub struct KeyPair {
    pub public: PublicKey,
    d: BigUint,
}

pub fn generate_key_pair() -> KeyPair {
    let (p, q) = generate_distinct_primes();
    let phi = (&p - BigUint::one()) * (&q - BigUint::one());
    let n = p * q;
    let e = 65537.to_biguint().unwrap();
    let d = mod_inverse(&e, &phi);
    KeyPair {
        public: PublicKey { n, e },
        d
    }
}

pub fn encrypt(message: &[u8], key: &PublicKey) -> Vec<u8> {
    let m = BigUint::from_bytes_le(message);
    m.modpow(&key.e, &key.n).to_bytes_le()
}

pub fn decrypt(message: &[u8], key: &KeyPair) -> Vec<u8> {
    let c = BigUint::from_bytes_le(message);
    c.modpow(&key.d, &key.public.n).to_bytes_le()
}


fn probably_prime(n: &BigUint) -> bool {
    const CHECKS: u8 = 3;
    let one = BigUint::one();
    let zero = BigUint::zero();
    let two = 2.to_biguint().unwrap();

    if *n < two {
        return false;
    }
    for small_prime in &[2, 3, 5, 7, 11, 13, 17, 19, 23, 29] {
        let small_prime = &small_prime.to_biguint().unwrap();
        if n % small_prime == zero {
            return small_prime == n;
        }
    }

    let mut s: u32 = 0;
    let mut d = n - &one;
    while (&d & &one) == zero {
        s += 1;
        d >>= 1;
    }

    let mut rng = rand::thread_rng();

    'outer:
    for _ in 0..CHECKS {
        let a: BigUint = rng.gen_biguint_range(&two, &(n - &one));
        let mut x = a.modpow(&d, &n);
        if x == one {
            continue;
        }
        for _ in 0..s {
            if x == (n - &one) {
                continue 'outer;
            }
            x = x.modpow(&two, &n);
        }
        return false;
    }
    true
}

fn generate_prime() -> BigUint {
    let mut rng = rand::thread_rng();
    loop {
        let mut candidate: BigUint = rng.sample(bigint::RandomBits::new(KEY_BITS));
        candidate |= BigUint::one();
        if probably_prime(&candidate) {
            return candidate;
        }
    }
}

fn generate_distinct_primes() -> (BigUint, BigUint) {
    let p = generate_prime();
    let mut q = generate_prime();
    while p == q {
        q = generate_prime();
    }
    (p, q)
}

fn egcd(a: &BigInt, b: &BigInt) -> (BigInt, BigInt, BigInt) {
    use num::Zero;
    use num::One;
    let mut a = a.clone();
    let mut b = b.clone();
    let mut x = BigInt::zero();
    let mut y = BigInt::one();
    let mut u = BigInt::one();
    let mut v = BigInt::zero();
    while a != BigInt::zero() {
        let q = &b / &a;
        let r = &b % &a;
        let m = &x - &u * &q;
        let n = &y - &v * &q;
        b = a; a = r;
        x = u; y = v;
        u = m; v = n;
    }
    (b, x, y)
}

fn mod_inverse(a: &BigUint, m: &BigUint) -> BigUint {
    let (gcd, x, _y) = egcd(&a.to_bigint().unwrap(), &m.to_bigint().unwrap());
    if gcd != BigInt::one() {
        panic!("Couldn't find inverse element");
    } else {
        (&x + m.to_bigint().unwrap()).to_biguint().unwrap() % m
    }
}

#[test]
fn test_mod_inverse() {
    use mod_inverse;
    use num::bigint::ToBigUint;

    assert_eq!(
        mod_inverse(
            &7.to_biguint().unwrap(),
            &12.to_biguint().unwrap()
        ), 7.to_biguint().unwrap()
    );

    const N: u32 = 2053;
    for i in 1..N {
        let inv = mod_inverse(
            &i.to_biguint().unwrap(),
            &N.to_biguint().unwrap()
        );
        assert_eq!((inv * i) % N, 1.to_biguint().unwrap());
    }
}

#[test]
fn test_probably_prime() {
    assert!(probably_prime(&5.to_biguint().unwrap()));
    assert!(!probably_prime(&6.to_biguint().unwrap()));
    assert!(probably_prime(&7.to_biguint().unwrap()));
    assert!(!probably_prime(&8.to_biguint().unwrap()));
    assert!(!probably_prime(&9.to_biguint().unwrap()));
    assert!(!probably_prime(&10.to_biguint().unwrap()));
    assert!(probably_prime(&11.to_biguint().unwrap()));
    assert!(!probably_prime(&12.to_biguint().unwrap()));
    assert!(probably_prime(&13.to_biguint().unwrap()));
    assert!(!probably_prime(&14.to_biguint().unwrap()));
    assert!(!probably_prime(&15.to_biguint().unwrap()));
    assert!(!probably_prime(&16.to_biguint().unwrap()));
    assert!(probably_prime(&17.to_biguint().unwrap()));
    assert!(!probably_prime(&18.to_biguint().unwrap()));
    assert!(probably_prime(&19.to_biguint().unwrap()));
    assert!(!probably_prime(&20.to_biguint().unwrap()));
    assert!(!probably_prime(&21.to_biguint().unwrap()));
    assert!(!probably_prime(&22.to_biguint().unwrap()));
    assert!(probably_prime(&23.to_biguint().unwrap()));
    assert!(probably_prime(&1031.to_biguint().unwrap()));
}

#[cfg(test)]
mod tests {
    use generate_key_pair;
    use encrypt;
    use decrypt;
    use KeyPair;

    fn assert_rsa_symmetry(message: &[u8], key_pair: &KeyPair) {
        let encrypted = encrypt(&message, &key_pair.public);
        let decrypted = decrypt(&encrypted, &key_pair);
        assert_eq!(message, decrypted.as_slice());
    }

    #[test]
    fn test_rsa() {
        let key_pair = generate_key_pair();
        let message: Vec<u8> = (0..250).collect();
        assert_rsa_symmetry(&message, &key_pair);
    }
}
