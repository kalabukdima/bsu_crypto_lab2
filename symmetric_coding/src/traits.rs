pub trait Encoder {
    const BLOCK_SIZE: usize;
    fn encode(text: &[u8], key: &[u8]) -> Vec<u8>;
}
