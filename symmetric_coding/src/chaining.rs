extern crate std;
use traits::Encoder;

fn output_feedback_encode_impl<E: Encoder>(
    text: &[u8],
    key: &[u8],
    init: &[u8],
) -> Vec<u8> {
    let mut result = Vec::<u8>::new();
    let mut last_output: Vec<u8> = init.to_owned();
    for chunk in text.chunks(E::BLOCK_SIZE) {
        result.extend(chunk.iter().zip(&last_output).map(
            |(x, y)| x ^ y
        ));
        last_output = E::encode(&last_output, key);
    }
    result
}

pub fn output_feedback_encode<E: Encoder>(text: &[u8], key: &[u8]) -> Vec<u8> {
    use std::iter::FromIterator;
    let v: Vec<u8> = Vec::from_iter(std::iter::repeat(0u8).take(E::BLOCK_SIZE));
    output_feedback_encode_impl::<E>(text, key, &v)
}

pub fn output_feedback_decode<E: Encoder>(text: &[u8], key: &[u8]) -> Vec<u8> {
    output_feedback_encode::<E>(text, key)
}
