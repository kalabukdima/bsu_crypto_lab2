use traits::Encoder;

pub struct AES {}

impl Encoder for AES {
    const BLOCK_SIZE: usize = 16;

    fn encode(text: &[u8], key: &[u8]) -> Vec<u8> {
        const ROUNDS: usize = 10;

        let mut text = text.to_owned();

        assert!(key.len() == Self::BLOCK_SIZE);
        assert!(text.len() == Self::BLOCK_SIZE);

        add_round_key(&mut text, &key);
        for _ in 0..ROUNDS {
            text = sub_bytes(&text);
            text = shift_rows(&text);
            text = mix_columns(&text);
            add_round_key(&mut text, &key);
        }

        text
    }
}

fn add_round_key(text: &mut [u8], key: &[u8]) {
    for (x, y) in text.into_iter().zip(key) {
        *x ^= y;
    }
}

fn sub_bytes(text: &[u8]) -> Vec<u8> {
    text.to_owned()
}

fn shift_rows(text: &[u8]) -> Vec<u8> {
    let indexes: Vec<usize> = vec![0, 1, 2, 3, 5, 6, 7, 4, 10, 11, 8, 9, 15, 12, 13, 14];
    indexes.iter().map(|&i| text[i]).collect()
}

fn mix_columns(text: &[u8]) -> Vec<u8> {
    // TODO Prettify
    let mut result = vec![0u8; 16];
    for j in 0..4 {
        let i0 = j;
        let i1 = j + 4;
        let i2 = j + 8;
        let i3 = j + 12;
        result[i0] = 2 * text[i0] + 3 * text[i1] +     text[i2] +     text[i3];
        result[i1] =     text[i0] + 2 * text[i1] + 3 * text[i2] +     text[i3];
        result[i2] =     text[i0] +     text[i1] + 2 * text[i2] + 3 * text[i3];
        result[i3] = 3 * text[i0] +     text[i1] +     text[i2] + 2 * text[i3];
    }
    result
}
