extern crate hyper;
extern crate rsa;
extern crate ron;
extern crate symmetric_coding;

use hyper::Client;
use hyper::rt::{self, Future, Stream};


const KEY_FILENAME: &str = "aes_key";

fn main() {
    let args: Vec<String> = std::env::args().skip(1).collect();
    match args.as_slice() {
        [url, command] if command == "get_key" => {
            rt::run(get_key(url));
        },
        [url, command, filename] if command == "file" => {
            rt::run(get_file(url, filename));
        },
        _ => {
            eprintln!("Usage:\nclient <url> get_key\nclient <url> file <filename>");
            std::process::exit(1);
        }
    }
}

fn get_key(base_url: &str) -> impl Future<Item=(), Error=()> {
    use std::io::Write;

    let rsa_key = rsa::generate_key_pair();

    let mut request = hyper::Request::new(
        hyper::Body::from(ron::ser::to_string(&rsa_key.public).unwrap())
    );
    let uri = format!("{}/session_key", base_url).parse().unwrap();
    *request.uri_mut() = uri;

    let client = Client::new();
    client
        .request(request)
        .map(move |res| {
            let body = res.into_body().map(|chunk| chunk.into_bytes())
                .concat2().wait().unwrap();
            let aes_key = rsa::decrypt(&body, &rsa_key);
            println!("Received aes key {:?}", &aes_key);

            let mut file = std::fs::File::create(
                std::path::Path::new(&KEY_FILENAME)
            ).unwrap();
            file.write_all(&aes_key).unwrap();
        })
        .map_err(|err| {
            eprintln!("Error {}", err);
        })
}

fn get_file(base_url: &str, filename: &str) -> impl Future<Item=(), Error=()> {
    use symmetric_coding::{output_feedback_decode, AES};

    let client = Client::new();
    client
        .get(format!(
            "{}/file?filename={}", &base_url, &filename
        ).parse().unwrap())
        .map(move |res| {
            let status = res.status();
            let body = res.into_body().map(
                |chunk| chunk.into_bytes()
            ).concat2().wait().unwrap();

            if !status.is_success() {
                eprintln!(
                    "{}: {}",
                    status.canonical_reason().unwrap(),
                    std::str::from_utf8(&body).unwrap()
                );
                return;
            }

            let aes_key = read_aes_key().expect(
                "Couldn't read AES key from file."
            );
            let message = output_feedback_decode::<AES>(&body, &aes_key);
            println!("{}", std::str::from_utf8(&message).expect(
                "Received file is not a valid UTF-8."
            ));
        })
        .map_err(|err| {
            eprintln!("Error {}", err);
        })
}


fn read_aes_key() -> std::io::Result<Vec<u8>> {
    read_file_contents(&std::path::PathBuf::from(KEY_FILENAME))
}

// TODO: reuse implementation.
fn read_file_contents(path: &std::path::PathBuf) -> std::io::Result<Vec<u8>> {
    use std::io::Read;

    let file = std::fs::File::open(path)?;
    let mut reader = std::io::BufReader::new(file);
    let mut contents = Vec::<u8>::new();
    reader.read_to_end(&mut contents)?;
    Ok(contents)
}
