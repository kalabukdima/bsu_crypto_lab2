extern crate iron;
extern crate router;
extern crate urlencoded;
extern crate rand;
#[macro_use] extern crate lazy_static;
extern crate ron;

extern crate symmetric_coding;
extern crate rsa;

use std::sync::Mutex;
use iron::prelude::*;
use iron::status;
use router::Router;
use urlencoded::UrlEncodedQuery;
use rand::prelude::*;


const KEY_LENGTH: usize = 16;
lazy_static! {
    static ref symmetric_key: Mutex<Option<Vec<u8>>> = Mutex::new(None);
}


fn main() {
    const PORT: u32 = 3000;

    let mut router = Router::new();
    router.get("/session_key", get_session_key, "session_key");
    router.get("/file", get_file, "file");

    let address = format!("localhost:{}", PORT);
    println!("Serving on http://{}...", address);
    Iron::new(router).http(address).unwrap();
}

fn get_session_key(request: &mut Request) -> IronResult<Response> {
    let client_pub_key = parse_pub_key(&mut request.body)
        .expect("Couldn't read client public RSA key.");

    let aes_key = generate_key();
    println!("Sending key {:?}", &aes_key);

    let mut response = Response::new();
    response.set_mut(status::Ok);
    response.set_mut(rsa::encrypt(&aes_key, &client_pub_key));

    Ok(response)
}

fn get_file(request: &mut Request) -> IronResult<Response> {
    let filename;
    match request.get_ref::<UrlEncodedQuery>() {
        Ok(ref hashmap) => {
            match hashmap.get("filename") {
                Some(vector) =>
                    filename = std::path::PathBuf::from(&vector[0]),
                None => return Ok(Response::with(status::BadRequest))
            }
        }
        Err(ref e) => return Ok(Response::with((
            status::BadRequest,
            format!("{}", e)
        )))
    };

    let contents = match read_file_contents(&filename) {
        Ok(contents) => contents,
        Err(ref e) => return Ok(Response::with((
            status::NotFound,
            format!("{}", e)
        )))
    };

    match encrypt(&contents) {
        Some(encrypted) => Ok(Response::with((status::Ok, encrypted))),
        None => Ok(Response::with((
            status::BadRequest,
            "Session key has not been generated."
        ))),
    }
}


fn parse_pub_key<R: std::io::Read>(reader: &mut R) -> Option<rsa::PublicKey> {
    let mut pub_key_serialized = String::new();
    match reader.read_to_string(&mut pub_key_serialized) {
        Ok(_) => (),
        Err(_) => return None,
    };

    let pub_key: rsa::PublicKey = match ron::de::from_str(&pub_key_serialized) {
        Ok(key) => key,
        Err(_) => return None,
    };
    Some(pub_key)
}

fn encrypt(text: &[u8]) -> Option<Vec<u8>> {
    use symmetric_coding::{output_feedback_encode, AES};
    let key = symmetric_key.lock().unwrap();
    match *key {
        Some(ref key) => Some(output_feedback_encode::<AES>(text, &key)),
        None => None
    }
}

fn read_file_contents(path: &std::path::PathBuf) -> std::io::Result<Vec<u8>> {
    use std::io::Read;

    let file = std::fs::File::open(path)?;
    let mut reader = std::io::BufReader::new(file);
    let mut contents = Vec::<u8>::new();
    reader.read_to_end(&mut contents)?;
    Ok(contents)
}

fn generate_key() -> Vec<u8> {
    let mut new_key = Vec::new();
    let mut rng = thread_rng();
    for _ in 0..KEY_LENGTH {
        let digit: u8 = rng.gen();
        new_key.push(digit);
    }

    let mut key = symmetric_key.lock().unwrap();
    *key = Some(new_key.clone());
    new_key
}
